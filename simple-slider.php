<?php
wp_enqueue_script('SoSlider', 'https://cdn.jsdelivr.net/gh/Socreativ-Bdx/SoSlider@1.5.1/SoSlider.min.js', array(), false, true);
wp_enqueue_style('soSlider', 'https://cdn.jsdelivr.net/gh/Socreativ-Bdx/SoSlider@1.5.1/SoSlider.min.css', [], '1.0', 'all');
?>



<?php if( have_rows('images') ): ?>
    <ul class="simple-slider list-unstyled">
        <?php while( have_rows('images') ): the_row(); 
            $image = get_sub_field('image');
            if( $image ):

                // Image variables.
                $url = $image['url'];
                $title = $image['title'];
                $alt = $image['alt'];
                $caption = $image['caption'];
            
                // Thumbnail size attributes.
                $size = 'thumbnail';
                $thumb = $image['sizes'][ $size ];
                $width = $image['sizes'][ $size . '-width' ];
                $height = $image['sizes'][ $size . '-height' ];

                if(my_wp_is_mobile()){
                    $size = 'thumbnail';
                    $thumb = $image['sizes'][ $size ];
                    $width = $image['sizes'][ $size . '-width' ];
                    $height = $image['sizes'][ $size . '-height' ];
                } else{
                    $size = 'thumbnail';
                    $thumb = $image['sizes'][ $size ];
                    $width = $image['sizes'][ $size . '-width' ];
                    $height = $image['sizes'][ $size . '-height' ];
                } ?>

                <li>
                    <figure>
                        <img src="<?php echo esc_url($image['url']); ?>"
                            alt="<?php echo esc_url($image['alt']); ?>" width="<?php echo $image['width']; ?>" height="<?php echo $image['height']; ?>">
                        <figcaption><?php the_sub_field('description'); ?></figcaption>
                    </figure>
                </li>
            <?php endif; ?>
        <?php endwhile; ?>
    </ul>
<?php endif; ?>