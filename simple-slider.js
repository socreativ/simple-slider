"use strict";
docReady(() => {
    let elements = document.querySelectorAll('.simple-slider');
    elements.forEach(element => {
        const slider = new SoSlider(element, {
            speed: 350,
            arrows: true,
            autoplay: false,
            infinite: true
        });
    });
});